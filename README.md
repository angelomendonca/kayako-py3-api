###Python 3 API wrapper for Kayako 4.01.240

### Updates:
* 1. Updated the api.py file to support Python 3
* 2. The bak files are Python 2 source files.

### Sample Example

```
from kayako import KayakoAPI
from kayako import User, Department, Staff, TicketType, TicketStatus, TicketPriority, Ticket


def create_ticket(**kwargs):

    api = KayakoAPI(API_URL, API_KEY, SECRET_KEY)
    bug = api.first(TicketType, title="Issue")
    status = api.first(TicketStatus, title="New")
    urgent = api.first(TicketPriority, title="Urgent")
    autouserid = kwargs.get("autouserid", None)
    department = api.first(Department, (), title='Kevlar Korma')

    args = {
        "tickettypeid": bug.id,
        "ticketstatusid": status.id,
        "ticketpriorityid": urgent.id,
        "departmentid": department.id,
    }

    if autouserid:
        args["autouserid"] = 1
    else:
        args["staffid"] = api.first(Staff, username=kwargs["staffname"]).id

    ticket = api.create(Ticket, **args)

    ticket.subject = 'Samosa Task Failure: Client ID %s,  File ID: %s, Location ID: %s' % \
                     (kwargs.get('client_id'), kwargs.get(
                         'file_id'), kwargs.get('location_id'))

    ticket.fullname = 'Angelo Mendonca'
    ticket.email = 'root@kievyan.io'
    ticket.contents = kwargs.get('message', 'Empty Message')
    ticket.add()

```
